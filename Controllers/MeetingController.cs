using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using mvcIdentity.Data;
using mvcIdentity.Models;

namespace mvcIdentity.Controllers
{
    public class MeetingController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MeetingController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Meeting
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Meeting.Include(m => m.Client);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Meeting/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meeting = await _context.Meeting
                .Include(m => m.Client)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (meeting == null)
            {
                return NotFound();
            }
            ViewData["Users"] = this.GetUsersMeeting(id);
            return View(meeting);
        }

        // GET: Meeting/Create
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            ViewData["Users"] = new SelectList(_context.Users.ToList<ApplicationUser>(), "Id", "UserName");
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Name");
            return View();
        }

        // POST: Meeting/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MeetingViewModel meeting)
        {
            int index;
            if (ModelState.IsValid)
            {
                if (meeting.IsVirtual == true)
                {
                    index = 3;
                }else
                {
                    index = 2;
                }
                var users = Request.Form.ToList();
                var meetingModel = new Meeting()
                {
                    Title = meeting.Title,
                    Datetime =meeting.Datetime,
                    IsVirtual = meeting.IsVirtual,
                    ClientId = meeting.ClientId
                };
                var result = _context.Add(meetingModel);
                await _context.SaveChangesAsync();
                
                foreach (var user in users[index].Value.ToList())
                {
                    var meet = new UserForMeeting()
                    {
                        UserId = user,
                        MeetingId  = meetingModel.Id
                    };
                    _context.UserForMeeting.Add(meet);
                }

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Name", meeting.ClientId);
            return View(meeting);
        }

        // GET: Meeting/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meeting = await _context.Meeting.SingleOrDefaultAsync(m => m.Id == id);
            if (meeting == null)
            {
                return NotFound();
            }
            var meet = new MeetingViewModel();
            meet.Id = meeting.Id;
            meet.IsVirtual = meeting.IsVirtual;
            meet.Datetime = meeting.Datetime;
            meet.Title = meeting.Title;
            meet.ClientId = meeting.ClientId;

            ViewData["Users"] = new SelectList(_context.Users.ToList<ApplicationUser>(), "Id", "UserName");
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Name", meeting.ClientId);
            return View(meet);
        }

        // POST: Meeting/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, MeetingViewModel meeting)
        {
            if (id != meeting.Id)
            {
                return NotFound();
            }
            var meetingModel = new Meeting();

            if (ModelState.IsValid)
            {
                try
                {
                    meetingModel = new Meeting()
                    {
                        Id = id,
                        Title = meeting.Title,
                        Datetime = meeting.Datetime,
                        IsVirtual = meeting.IsVirtual,
                        ClientId = meeting.ClientId
                    };
                    var result = _context.Update(meetingModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MeetingExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                var users = Request.Form.ToList();
                foreach (var user in users[2].Value.ToList())
                {
                    var me = new UserForMeeting()
                    {
                        UserId = user,
                        MeetingId = meetingModel.Id
                    };
                    var meetingsForUser = (from mu in _context.UserForMeeting
                                           where mu.MeetingId == id && mu.UserId == user
                                           select new UserForMeeting
                                           {
                                               Id = mu.Id,
                                               MeetingId = mu.MeetingId,
                                               UserId = mu.UserId
                                           }).ToList();
                    if (meetingsForUser.Count == 0)
                    {
                        _context.UserForMeeting.Add(me);
                        _context.SaveChangesAsync();
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Name", meeting.ClientId);
            return View(meeting);
        }

        // GET: Meeting/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meeting = await _context.Meeting
                .Include(m => m.Client)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (meeting == null)
            {
                return NotFound();
            }

            return View(meeting);
        }

        // POST: Meeting/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _context.Database.ExecuteSqlCommand("Delete from UserForMeeting where MeetingId = {0}", id);
            var meeting = await _context.Meeting.SingleOrDefaultAsync(m => m.Id == id);
            _context.Meeting.Remove(meeting);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public IEnumerable<ApplicationUser> GetUsersMeeting(int id)
        {
            var meetingsForUser =  (from mu in _context.UserForMeeting
                                    where mu.MeetingId == id
                                    select new UserForMeeting
                                    {
                                        Id = mu.Id,
                                        MeetingId = mu.MeetingId,
                                        UserId = mu.UserId
                                    }).ToList();
            MeetingViewModel U = new MeetingViewModel(); 
            foreach (var item in meetingsForUser)
            {
               U.Users.Add((_context.Users.Find(item.UserId)));
            }
            IEnumerable<ApplicationUser> users = U.Users;
            return users;
        }

        private bool MeetingExists(int id)
        {
            return _context.Meeting.Any(e => e.Id == id);
        }
    }
}
