using System;
using System.ComponentModel.DataAnnotations;

namespace mvcIdentity.Models
{
    public class Meeting
    {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 3)]
        [Required]
        public string Title { get; set; }
        [Display(Name = "Date and Time"),DataType(DataType.DateTime)]
        [Required]
        public DateTime Datetime { get; set; }
        [Display(Name = "Is Virtual?")]
        public Boolean IsVirtual { get; set; }
        public int? ClientId { get; set; }
        public virtual Client Client { get; set; }

    }
}