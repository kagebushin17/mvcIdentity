using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace mvcIdentity.Models
{
    public class MeetingViewModel
    {
        public MeetingViewModel()
        {
            Title = "";
            Datetime = new DateTime();
            IsVirtual = false;
            Users = new List<ApplicationUser>();
        }
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 3)]
        [Required]
        public string Title { get; set; }
        [Required]
        public List<ApplicationUser> Users {get; set;}

        [Display(Name = "Date and Time"),DataType(DataType.DateTime)]
        [Required]
        public DateTime Datetime { get; set; }
        [Display(Name = "Is Virtual")]
        public Boolean IsVirtual { get; set; }
        [Display(Name = "Client")]
        public int? ClientId { get; set; }
        public virtual Client Client { get; set; }

    }
}