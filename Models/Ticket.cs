using System;
using System.ComponentModel.DataAnnotations;

namespace mvcIdentity.Models
{
    public class Ticket
    {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 3)]
        [Required]
        public string Title { get; set; }
        [Required]
        public string Details { get; set; }
        [Required]
        public string Reporter { get; set; }
        [Required]
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
        [Required]
        public Status Status { get; set; }
    }
    public enum Status {
        Open, Process, Waiting, Closed
    };
}