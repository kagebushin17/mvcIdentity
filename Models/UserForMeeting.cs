using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace mvcIdentity.Models
{
    public class UserForMeeting
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public int MeetingId { get; set; }
        public virtual Meeting Meeting { get; set; }
        

        

    }
}